<?php
/**
* Sign up a new user
* 
* Form to register a username in order to use the forum. The user cannot post replies to topics or make new topics 
* without being signed up.
* 
* @author Meri Alho
* @package forum
*/

include 'connect.php';
include 'header.php';
 
echo '<h3>Sign up</h3>';
 
if($_SERVER['REQUEST_METHOD'] != 'POST'){
    /*the form hasn't been posted yet, display it */
      
    echo '<form method="post" action="" name="signup" onsubmit="return formcheck()">
        Username: <input type="text" name="user_name" /><br><br>
        Password: <input type="password" name="user_pass"><br><br>
        Password again: <input type="password" name="user_pass_check"><br><br>
        E-mail: <input type="email" name="user_email"><br><br>
        <input type="submit" value="Sign up" />
     </form>';
}
else{
    /*the form has been posted, we'll process the data in three steps:
        1.  Check the data
        2.  Let the user refill the wrong fields (if necessary)
        3.  Save the data 
    */
    $errors = array(); /* array for later use */
    
    //remove html tags
    /** @var string $postusername   Removing all hmtl tags from reply content. Reply content gotten from form with POST-method. */
    $postusername = strip_tags($_POST['user_name']);
     
    if(isset($postusername)){
        //if the user name exists
        if(!ctype_alnum($postusername)){
            $errors[] = 'The username can only contain letters and digits.';
        }
        if(strlen($postusername) > 30){
            $errors[] = 'The username cannot be longer than 30 characters.';
        }
    }
    else{
        $errors[] = 'The username field must not be empty.';
    }
    
    //remove html tags
    /** @var string $postuserpass   Removing all hmtl tags from reply content. Reply content gotten from form with POST-method. */
    $postuserpass = strip_tags($_POST['user_pass']);
    
    /** @var string $userpasscheck   Removing all hmtl tags from reply content. Reply content gotten from form with POST-method. */
    $userpasscheck = strip_tags($_POST['user_pass_check']);
     
    if(isset($postuserpass)){
        if($postuserpass != $userpasscheck){
            $errors[] = 'The two passwords did not match.';
        }
    }
    else{
        $errors[] = 'The password field cannot be empty.';
    }
     
    if(!empty($errors)){ /*check for an empty array*/
    
        echo 'Some fields are not filled in correctly..';
        echo '<ul>';
        foreach($errors as $key => $value){ /* walk through the array so all the errors get displayed */
        
            echo '<li>' . $value . '</li>'; /* generates an error list */
        }
        echo '</ul>';
    }
    else{
        //the form has been posted without errors, so save it
        //mysqli_real_escape_string to strip special chars
        //sha1 function hashes the password (not the best option to use in real life
        //but adequately sufficient for this project)
        
        /** @var string $useremail   Removing all hmtl tags from reply content. Reply content gotten from form with POST-method. */
        $useremail = strip_tags($_POST['user_email']);
        
        /** @var string $sql    MySQL query string. Inserts user data into database. */
        $sql = "INSERT INTO
                    users(user_name, user_pass, user_email ,user_date, user_level)
                VALUES('" . mysql_real_escape_string($postusername) . "',
                       '" . sha1($postuserpass) . "',
                       '" . mysql_real_escape_string($useremail) . "',
                        NOW(),
                        0)";
        
        /** @var string $result    Result for MySQL query */                 
        $result = mysql_query($sql);
        if(!$result){
            //something went wrong, display the error
            echo 'Something went wrong while registering. Please try again later.';
            //echo mysql_error(); //debugging purposes, uncomment when needed
        }
        else{
            echo 'Successfully registered. You can now <a href="signin.php">sign in</a> and start posting! :)';
        }
    }
}
 
include 'footer.php';
?>

<script>
    
    /**
     * Check the if the form has content. If all fields are not filled correctly, display an error and stop
     * form from sending. 
    */
    function formcheck(){
        var x = document.forms["signup"]["user_name"].value;
        if (x == null || x == "") {
            alert("You must fill in username");
            return false;
        }
        
        var y = document.forms["signup"]["user_pass"].value;
        if (y == null || y == "") {
            alert("You must fill in password");
            return false;
        }
        
        var z = document.forms["signup"]["user_pass_check"].value;
        if (z == null || z == "") {
            alert("You must fill in the password check -field");
            return false;
        }
        
        var n = document.forms["signup"]["user_email"].value;
        if (n == null || n == "") {
            alert("You must fill in an email address");
            return false;
        }

    }
    
</script>
