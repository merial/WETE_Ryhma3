<?php
/**
 * Topic reply saving
 * 
 * This file saves the reply to topic which has been made in replyform.php.
 * 
* @author Meri Alho
* @package forum
*/


include 'connect.php';
include 'header.php';

/** Continue signed in -session */
session_start();

 
if($_SERVER['REQUEST_METHOD'] != 'POST'){
    //someone is calling the file directly, which is not allowed
    echo 'This file cannot be called directly.';
}
else{
    //check for signin status
    if(!$_SESSION['signed_in']){
        echo 'You must be signed in to post a reply.';
    }
    else{
        //remove all html tags
        /** @var string $replycontent   Removing all hmtl tags from reply content. Reply content gotten from form with POST-method. */
        $replycontent = strip_tags($_POST['reply-content']);
        
        /** @var string $sql    MySQL query string */
        $sql = "INSERT INTO 
                    posts(post_content,
                          post_date,
                          post_topic,
                          post_by) 
                VALUES ('" . $replycontent . "',
                        NOW(),
                        " . mysql_real_escape_string($_GET['id']) . ",
                        " . $_SESSION['user_id'] . ")";
         
        /** @var string $result     Result gotten from the MySQL query. */                
        $result = mysql_query($sql);
                         
        if(!$result){
            echo 'Your reply has not been saved, please try again later.';
        }
        else{
            echo 'Your reply has been saved, check out <a href="topic.php?id=' . $_GET['id'] . '">the topic</a>.';
        }
    }
}
 
include 'footer.php';
?>
