<?php
/**
* Database connection
* 
* Try to create a connection to MySQL database. If it does not succeed, display an error message.
* 
* @author Meri Alho
* @package forum
*/

/** @var string $server     Name of the server to be used 
* @var string $username     Username for the database
* @var string $password     Password for the database
* @var string $database     Name of the database to be used
*/
$server = 'localhost';
$username   = 'discuss_forum';
$password   = 'discuss_forum';
$database   = 'discuss_forum';
 
if(!mysql_connect($server, $username,  $password)){
    exit('Error: could not establish database connection');
    }
    
    
if(!mysql_select_db($database)){
    exit('Error: could not select the database');
}




?>
