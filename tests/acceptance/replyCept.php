<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('reply as regular user');
$I->amOnPage('reply');
$I->click('POST');
$I->see('Your reply has not been saved, please try again later.');
?>