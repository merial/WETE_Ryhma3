<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('sign up as regular user');
$I->amOnPage('signup');
$I->fillField('Username','user_name');
$I->fillField('Password','user_pass');
$I->fillField('Password','user_pass_check');
$I->fillField('email','user_email');
$I->click('Sign up');
$I->see('Successfully registered. You can now <a href="signin.php">sign in</a> and start posting! :)');
?>