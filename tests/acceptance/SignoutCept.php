<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('log out as regular user');
$I->amOnPage('signout');
$I->see('You have signed out. <br><br>');
?>