<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('log in as regular user');
$I->amOnPage('signed_in');
$I->see('You are already signed in, you can <a href="signout.php">sign out</a> if you want.');
$I->amONPage('Sign in');
$I->fillField('Username','user_name');
$I->fillField('Password','qwerty');
$I->click('Sign in');
$I->see('Welcome, user_name');
?>