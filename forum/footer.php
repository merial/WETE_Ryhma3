<?php
/**
* Footer for all pages
* 
* This file generates the footer part of the webpage. This is used in almost all the other files.
* 
* @author Meri Alho
* @package forum
*/

/** 
 * Only html content
*/
?>

</div><!-- content -->
</div><!-- wrapper -->
<div id="footer">FOOTER</div>
</body>
</html>