<?php
/**
 * User sign in
 * 
 * User can sign in with a username that already exists in the database.
 * 
 * @author Meri Alho
 * @package forum
 */

include 'connect.php';
include 'header.php';
 
/** Continue signed in -session */
session_start();

echo '<h3>Sign in</h3>';
 
//first, check if the user is already signed in. If that is the case, there is no need to display this page
if(isset($_SESSION['signed_in']) && $_SESSION['signed_in'] == true){
    echo 'You are already signed in, you can <a href="signout.php">sign out</a> if you want.';
}
else{
    if($_SERVER['REQUEST_METHOD'] != 'POST'){
        /*the form hasn't been posted yet, display it*/
        
        echo '<form method="post" action="" onsubmit="return formcheck()" name="signin">
            Username: <input type="text" name="user_name" />
            Password: <input type="password" name="user_pass">
            <input type="submit" value="Sign in" />
         </form>';
    }
    else{
        /* the form has been posted, we'll process the data in three steps:
            1.  Check the data
            2.  Let the user refill the wrong fields (if necessary)
            3.  Verify if the data is correct and return the correct response
        */
        $errors = array(); /* declare the array for later use */
         
        if(!isset($_POST['user_name'])){
            $errors[] = 'The username field must not be empty.';
        }
         
        if(!isset($_POST['user_pass'])){
            $errors[] = 'The password field must not be empty.';
        }
         
        if(!empty($errors)) /*check for an empty array, if there are errors, they're in this array*/
        {
            echo 'Some fields are not filled in correctly..';
            echo '<ul>';
            foreach($errors as $key => $value) /* walk through the array so all the errors get displayed */
            {
                echo '<li>' . $value . '</li>'; /* generates a nice error list */
            }
            echo '</ul>';
        }
        else{
            //the form has been posted without errors, so save it
            //the sha1 function which hashes the password (as noted in signup, not the best option for real use,
            // but sufficient in this project)
            //strip html tags
            
            /** @var string $postusername   Removing all hmtl tags from reply content. Reply content gotten from form with POST-method. */
            $postusername = strip_tags($_POST['user_name']);
            
            /** @var string $postuserpass   Removing all hmtl tags from reply content. Reply content gotten from form with POST-method. */
            $postuserpass = strip_tags($_POST['user_pass']);
            
            /** @var string $sql    MySQL query string */
            $sql = "SELECT 
                        user_id,
                        user_name,
                        user_level
                    FROM
                        users
                    WHERE
                        user_name = '" . mysql_real_escape_string($postusername) . "'
                    AND
                        user_pass = '" . sha1($postuserpass) . "'";
            
            /** @var string $result     Result gotten from the MySQL query. */             
            $result = mysql_query($sql);
            if(!$result){
                //something went wrong, display the error
                echo 'Something went wrong while signing in. Please try again later.';
            }
            else{
                //the query was successfully executed, there are 2 possibilities
                //1. the query returned data, the user can be signed in
                //2. the query returned an empty result set, the credentials were wrong
                if(mysql_num_rows($result) == 0)
                {
                    echo 'You have supplied a wrong user/password combination. Please try again.';
                }
                else{
                    //set the $_SESSION['signed_in'] variable to TRUE
                    $_SESSION['signed_in'] = true;
                     
                    //we also put the user_id and user_name values in the $_SESSION, so we can use it at various pages
                    while($row = mysql_fetch_assoc($result)){
                        $_SESSION['user_id']    = $row['user_id'];
                        $_SESSION['user_name']  = $row['user_name'];
                        $_SESSION['user_level'] = $row['user_level'];
                    }
                     
                    echo 'Welcome, ' . $_SESSION['user_name'] . '. <a href="index.php">Proceed to the forum overview</a>.';
                }
            }
        }
    }
}
 
include 'footer.php';
?>

<script>
    
    /**
     * Check the if the form has content. If all fields are not filled correctly, display an error and stop
     * form from sending.
    */
    function formcheck(){
        var x = document.forms["signin"]["user_name"].value;
        if (x == null || x == "") {
            alert("You must fill in username");
            return false;
        }
        var y = document.forms["signin"]["user_pass"].value;
        if (y == null || y == "") {
            alert("You must fill in password");
            return false;
        }

    }
    
</script>