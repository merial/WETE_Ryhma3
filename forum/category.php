<?php
/**
* Categories display
* 
* This file displays all the topics included in a chosen category and places them in a table. 
* From here you can access all the topics in the category.
* 
* @author Meri Alho
* @package forum
*/

/** include mysql connection and header */
include 'connect.php';
include 'header.php';

/** Continue signed in -session */
session_start();

 
/** @var string $sql    MySQL querystring to get the chosen category. Select the category based on $_GET['id']. */
$sql = "SELECT
            cat_id,
            cat_name,
            cat_description
        FROM
            categories
        WHERE
            cat_id =' " . mysql_real_escape_string($_GET['id']). "'";

/** @var string $result     The result of the previous MySQL query */ 
$result = mysql_query($sql);
 
if(!$result){
    echo 'The category could not be displayed, please try again later.' . mysql_error();
}
else{
    if(mysql_num_rows($result) == 0){
        echo 'This category does not exist.';
    }
    else{
        //display category data
        while($row = mysql_fetch_assoc($result)){
            echo '<h2>Topics in ' . $row['cat_name'] . ' </h2>';
        }
        echo '<a href="index.php">Back to categories</a> | <a href="create_topic.php">Create a new topic</a><br><br>' ;
     
        //do a query for the topics
        /** @var string $sql    MySQL querystring to get the topics information in the chosen category. Select the category based on $_GET['id']. */
        $sql = "SELECT  
                    topic_id,
                    topic_subject,
                    topic_date,
                    topic_cat,
                    topic_by
                FROM
                    topics
                WHERE
                    topic_cat = " . mysql_real_escape_string($_GET['id']);
        
        /** @var string $result     The result of the previous MySQL query */  
        $result = mysql_query($sql);
         
        if(!$result){
            echo 'The topics could not be displayed, please try again later.';
        }
        else{
            if(mysql_num_rows($result) == 0){
                echo 'There are no topics in this category yet.';
            }
            else{
                //prepare the table
                echo '<table border="1">
                      <tr>
                        <th>Topic</th>
                        <th>Posts</th>
                        <th>Created at</th>
                      </tr>'; 
                     
                while($row = mysql_fetch_assoc($result)){               
                    echo '<tr>';
                        echo '<td class="leftpart">';
                            echo '<h3><a href="topic.php?id=' . $row['topic_id'] . '">' . $row['topic_subject'] . '</a><h3>';
                        echo '</td>';
                        
                    /** @var string $msgcount       MySQL query string to get the number of posts in each topic */
                    $msgcount = "SELECT
                                    post_topic
                                FROM
                                    posts
                                WHERE
                                    post_topic =" . $row['topic_id'];
                    
                    /** @var string $messages       The result of the previous MySQL query. Result is the number of posts made in a spesific topic */        
                    $messages = mysql_query($msgcount);          
                    
                    echo '<td>'. mysql_num_rows($messages) . '</td>';
                    
                    /** @var string $sqluser       MySQL query string to get the username of the last user who posted in a topic */    
                    $sqluser = "SELECT
                                    user_name
                                FROM
                                    users
                                WHERE
                                    user_id = " . $row['topic_by'];
                                    
                    /** @var string $resultuser       The result of the previous MySQL query.  */      
                    $resultuser = mysql_query($sqluser);
                        
                    while($rowuser = mysql_fetch_assoc($resultuser)){
                        /** @var string $byuser         Select the username from the MySQL query result associative array  */
                        $byuser = $rowuser['user_name'];
                    }
                        
                        echo '<td class="rightpart">';
                            echo date('d-m-Y H:i:s', strtotime($row['topic_date']));
                            echo "<br>By: ". $byuser;
                        echo '</td>';
                    echo '</tr>';
                }
            }
        }
    }
}
 

include 'footer.php';
?>