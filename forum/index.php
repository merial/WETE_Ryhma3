<?php
/**
* Main page
* 
* The main page of the forum. Displays the selectable categories information and the time and creator of the latest topic
* post in the category.
* 
* @author Meri Alho
* @package forum
*/

include 'connect.php';
include 'header.php';

/** Continue signed in -session */
session_start();

/** @var string $sql    MySQL query string. Get category information and date of latest topic in given category. */         
$sql = "SELECT
            cat_id,
            cat_name,
            cat_description,
            topic_date
        FROM
            categories, topics
        GROUP BY
            categories.cat_id";
            
/** @var string $result     Result of MySQL query.  */
$result = mysql_query($sql);

 
if(!$result){
    echo 'The categories could not be displayed, please try again later.';
}
else{
    if(mysql_num_rows($result) == 0){
        echo 'No categories defined yet.';
    }
    else{
        //prepare the table
        echo '<table border="1">
              <tr>
                <th>Category</th>
                <th>Topics</th>
                <th>Last topic</th>
              </tr>'; 
             
        while($row = mysql_fetch_assoc($result)){ 
            echo '<tr>';
                
                echo '<td class="leftpart">';
                    echo '<h3><a href="category.php?id='. $row['cat_id'] . '">' . $row['cat_name']. ' </a></h3>';
                    echo $row['cat_description'];
                echo '</td>';
            
            /** @var string $sql    MySQL query string to be excecuted. Retrieves topic category information */
            $sqlcount = "SELECT
                            topic_cat
                        FROM
                            topics
                        WHERE
                            topic_cat =" . $row['cat_id'];
            
            /** @var string $topiccount     Result gotten from the MySQL query. */                
            $topiccount = mysql_query($sqlcount);          
            
            echo '<td>'. mysql_num_rows($topiccount) . '</td>';

            /** @var string $sqltopic    MySQL query string to be excecuted. Retrieves topic date information */
            $sqltopic = "SELECT
                            topic_date
                        FROM
                            topics
                        WHERE
                            topic_cat = " . $row['cat_id'];
                            
            /** @var string $resulttopic     Result gotten from the MySQL query. */
            $resulttopic = mysql_query($sqltopic);


            if (mysql_num_rows($resulttopic) > 0){
                
                while ($rowtopic = mysql_fetch_assoc($resulttopic)){
                    /** @var string $topicdate      Get topic_date from SQL associative result array  */
                    $topicdate = $rowtopic['topic_date'];
                }
                        
                echo '<td class="rightpart">';
                    echo 'Latest topic: '. date('d-m-Y H:i:s', strtotime($topicdate));
                echo '</td>';
            echo '</tr>';
           
            }
            else{
                echo '<td class="rightpart">';
                echo "No topics yet";
            }


                
            
        }
    }
    
}

?>

