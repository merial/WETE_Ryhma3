-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Palvelin: 127.0.0.1
-- Luontiaika: 09.05.2016 klo 08:28
-- Palvelimen versio: 5.5.47-0ubuntu0.14.04.1
-- PHP:n versio: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Tietokanta: `discuss_forum`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(8) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  `cat_description` varchar(255) NOT NULL,
  PRIMARY KEY (`cat_id`),
  UNIQUE KEY `cat_name_unique` (`cat_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Vedos taulusta `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_description`) VALUES
(1, 'Music', 'Here you can talk about music'),
(4, 'Food', 'Recipes and food pix and stuff'),
(6, 'ROSKAKORI', 'Roge hommat'),
(8, 'Testing', 'testing tesintginignsfsldkfÃ¶fk Ã¶lfdkaÃ¶lsdk'),
(9, 'General', 'The general category'),
(11, 'Sports', 'Sport related stuff');

-- --------------------------------------------------------

--
-- Rakenne taululle `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(8) NOT NULL AUTO_INCREMENT,
  `post_content` text NOT NULL,
  `post_date` datetime NOT NULL,
  `post_topic` int(8) NOT NULL,
  `post_by` int(8) NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `post_topic` (`post_topic`),
  KEY `post_by` (`post_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Vedos taulusta `posts`
--

INSERT INTO `posts` (`post_id`, `post_content`, `post_date`, `post_topic`, `post_by`) VALUES
(1, 'and how does it work', '2016-04-27 06:31:17', 1, 1),
(2, 'yum yum n stuff', '2016-04-27 08:26:23', 2, 4),
(9, 'ssaSasAS', '2016-04-27 09:29:44', 1, 4),
(10, 'juuh elikkÃ¤s', '2016-04-27 09:42:04', 3, 4),
(11, 'Moi kaikki', '2016-04-28 10:11:33', 4, 1),
(12, 'no niin elikkÃ¤s sadasdDSADDFDFDSF////', '2016-04-28 10:25:30', 5, 1),
(13, 'bsdfbghheg', '2016-04-28 10:56:00', 6, 1),
(14, 'YES', '2016-04-28 10:58:35', 6, 1),
(15, 'Semmone tÃ¤mmÃ¶ne ', '2016-04-28 11:07:47', 7, 5),
(16, 'NO NIIN ', '2016-04-29 08:04:06', 3, 1),
(17, 'TÃ¤mmÃ¶ne', '2016-04-29 08:04:24', 3, 1),
(18, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat a magna at lobortis. In ut mauris et nibh tristique dignissim. Proin interdum mattis pharetra. Maecenas et dictum risus, et sodales lacus. Donec non euismod lectus. Nam tristique fringilla metus ut efficitur. Vestibulum ac tempus metus. Ut porttitor vehicula vestibulum. Suspendisse pretium massa scelerisque, bibendum metus eget, aliquam mauris. Cras nibh massa, porta a justo vitae, sagittis tristique leo. Etiam nec purus elementum, viverra ex vel, eleifend odio. Aliquam erat volutpat. Etiam accumsan auctor rhoncus.\r\n\r\nPhasellus tincidunt commodo imperdiet. Fusce dignissim quam vitae mauris sodales, eget venenatis lacus sollicitudin. Suspendisse potenti. Suspendisse sollicitudin ac nisi et sagittis. Sed eros eros, porttitor et iaculis eget, dignissim consequat urna. Vivamus erat nisl, bibendum id nibh nec, varius ultricies purus. Etiam quam diam, ultrices id imperdiet sed, fermentum at leo. Sed fermentum id mauris quis suscipit. Ut vestibulum rutrum sapien, eget semper massa ullamcorper fringilla. Ut tristique urna ut consectetur imperdiet. Morbi hendrerit tellus nec dictum venenatis. Donec non tincidunt libero. Aliquam eu libero egestas, convallis quam non, egestas magna. Curabitur accumsan interdum risus ut sodales. Mauris blandit nisl ut felis porta suscipit.\r\n\r\nVestibulum suscipit diam et erat tincidunt, vel posuere lectus ultricies. Aenean a ex magna. Praesent pretium massa sed ligula egestas, a convallis magna porttitor. Sed vel turpis malesuada, laoreet lorem non, placerat elit. Phasellus vitae ex sit amet turpis ullamcorper vulputate eleifend sodales purus. Fusce pulvinar non erat eget dapibus. Praesent auctor, velit id rhoncus congue, mi dui elementum velit, nec hendrerit magna eros ut elit. Nullam et convallis est. Mauris posuere ullamcorper fermentum. Nulla sit amet iaculis ex. Nunc in iaculis tellus. Etiam malesuada ligula eget turpis laoreet, a finibus lorem eleifend. Curabitur ornare velit erat, ut fermentum enim consequat vitae.\r\n\r\nCurabitur iaculis et lectus in luctus. Sed eu pretium felis. Nulla finibus tristique molestie. Integer porta aliquam mauris, in tincidunt dolor lobortis a. Suspendisse nunc diam, tincidunt non metus a, mollis molestie est. Pellentesque dui sem, tempus ut arcu ut, iaculis congue erat. Cras congue leo turpis, eu pulvinar lacus lacinia sit amet. Nam ut mauris justo. Aliquam cursus mollis hendrerit. Integer hendrerit, augue eget tristique facilisis, tellus ligula porttitor purus, quis pretium nulla metus vehicula massa. Etiam malesuada ante condimentum diam lacinia tristique. Phasellus quis enim id ante pellentesque bibendum eu id ex. Integer non mi posuere, venenatis nibh in, fermentum massa. Sed tristique placerat augue non sodales.\r\n\r\nQuisque diam augue, suscipit eu magna nec, luctus ullamcorper metus. Duis at sollicitudin felis. Suspendisse potenti. Mauris ac ornare mi, vitae efficitur ante. Mauris ultrices sodales mollis. Duis ante sem, dictum quis justo eget, maximus gravida lorem. Donec in porta mi, et efficitur ligula. Nunc eget vehicula felis. Vivamus massa nulla, ultricies et semper at, tincidunt maximus justo. Duis ante justo, fringilla sed ipsum at, semper semper ante. Duis maximus lectus in lectus iaculis, ut vestibulum ligula aliquet. Vestibulum in pretium nisi. Pellentesque placerat risus non suscipit rutrum.', '2016-04-29 08:05:16', 3, 1),
(19, 'juuuuuufiusdfifuudif', '2016-05-02 07:23:50', 8, 1),
(20, 'shittt', '2016-05-02 11:15:08', 5, 1),
(21, 'fsfgsf fgggdgdfg g ', '2016-05-02 11:23:03', 5, 1),
(22, 'REplYYYYYY ass', '2016-05-04 08:23:13', 1, 1),
(23, 'sdadfDASASDASD', '2016-05-04 09:51:27', 3, 1),
(24, 'i duly  agree', '2016-05-09 08:23:40', 2, 1);

-- --------------------------------------------------------

--
-- Rakenne taululle `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `topic_id` int(8) NOT NULL AUTO_INCREMENT,
  `topic_subject` varchar(255) NOT NULL,
  `topic_date` datetime NOT NULL,
  `topic_cat` int(8) NOT NULL,
  `topic_by` int(8) NOT NULL,
  PRIMARY KEY (`topic_id`),
  KEY `topic_cat` (`topic_cat`),
  KEY `topic_by` (`topic_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Vedos taulusta `topics`
--

INSERT INTO `topics` (`topic_id`, `topic_subject`, `topic_date`, `topic_cat`, `topic_by`) VALUES
(1, 'what is music', '2016-04-27 06:31:17', 1, 1),
(2, 'Food is good', '2016-04-27 08:26:23', 4, 4),
(3, 'moontÃ¤s', '2016-04-27 09:42:04', 6, 4),
(4, 'Uusi topi', '2016-04-28 10:11:33', 6, 1),
(5, 'lisÃ¤topik', '2016-04-28 10:25:30', 6, 1),
(6, 'aaaaaaaaa', '2016-04-28 10:56:00', 4, 1),
(7, 'HERE I TESTING', '2016-04-28 11:07:47', 6, 5),
(8, 'Asia', '2016-05-02 07:23:50', 6, 1);

-- --------------------------------------------------------

--
-- Rakenne taululle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(8) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_date` datetime NOT NULL,
  `user_level` int(8) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name_unique` (`user_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Vedos taulusta `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_pass`, `user_email`, `user_date`, `user_level`) VALUES
(1, 'meri', 'a7cf451ffd30065aa962666cd36e4d3c9f16b6aa', 'meri.alho@gmail.com', '2016-04-25 13:46:06', 1),
(3, 'simpanssi', '7e240de74fb1ed08fa08d38063f6a6a91462a815', 'simo.simpanssi@jotain.com', '2016-04-25 13:55:19', 0),
(4, 'urpo', 'ce62870bd8089649d0c74bde58dc9fd4b7f49f54', 'urpo@naama.fi', '2016-04-25 15:19:20', 0),
(5, 'TEST', 'f1e1c6ea766397606475ab41d7f124258da887b9', 'testi@testinen.com', '2016-04-28 11:06:52', 0),
(6, 'vvv', '7e240de74fb1ed08fa08d38063f6a6a91462a815', 'aaa@bbb.com', '2016-05-04 10:14:38', 0);

--
-- Rajoitteet vedostauluille
--

--
-- Rajoitteet taululle `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`post_topic`) REFERENCES `topics` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`post_by`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Rajoitteet taululle `topics`
--
ALTER TABLE `topics`
  ADD CONSTRAINT `topics_ibfk_1` FOREIGN KEY (`topic_cat`) REFERENCES `categories` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `topics_ibfk_2` FOREIGN KEY (`topic_by`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
