<?php
/**
* Create a new category. 
* 
* This can be done only if the user has admin privileges (user level 1). User level can be changed only straight from the database. 
* 
* @author Meri Alho
* @package forum
*/

include 'header.php';
include 'connect.php';

/** Continue signed in -session */
session_start();


echo '<h2>Create a category</h2>';

if($_SESSION['signed_in'] == false){
    //the user is not signed in
    echo 'Sorry, you have to be <a href="signin.php">signed in</a> to create a new category.';
}

else{ 
    
    /** 
     * @var string $sql     Query string to be excecuted in MySQL. Gets the users level using session variable for user id. 
    */
    
    $sql = "SELECT
            user_level
        FROM
            users
        WHERE
            user_id =' " . $_SESSION['user_id']. "'";
 
    /** @var string $result     MySQL result for query  */
    $result = mysql_query($sql);
    
    while ($row = mysql_fetch_assoc($result)){
        
        /** @var string $userlevel picks the result from MySQL response for user_level from wanted user   */
        $userlevel = $row['user_level'];
    }
    
    if ($userlevel==1){

        if($_SERVER['REQUEST_METHOD'] != 'POST'){
            
            //the form hasn't been posted yet, display it
            echo "<form method='post' action='' name='category' onsubmit='return formcheck()'>
                Category name: <input type='text' name='cat_name' /><br><br>
                Category description: <br><textarea name='cat_description' /></textarea><br>
                <input type='submit' value='Add category'/>
             </form>";
        }
        else{

            //remove html tags            
            /** @var string catname     Get category name from form with POST-method */
            $catname = strip_tags($_POST['cat_name']);
            
            /** @var string catdesc     Get category description from form with POST-method */
            $catdesc = strip_tags($_POST['cat_description']);
            
            //the form has been posted, so save it
            /** @var string $sql    String to insert the acquired data to MySQL */
            $sql = "INSERT INTO 
                        categories(cat_name, cat_description)
                    VALUES('" . mysql_real_escape_string($catname) . "',
                     '" . mysql_real_escape_string($catdesc) . "')";
            
            /**  @var string $result    Result for MySQL query */
            $result = mysql_query($sql);
            
            if(!$result){
                //something went wrong, display the error
                echo 'Error ' . mysql_error();
            }
            else{
                echo 'New category successfully added.';
            }
        }
    }
    else{
        echo 'Sorry, you have to be a forum moderator to create a new category!';
    }
    
}
include 'footer.php';
?>

<script>

    /**
     * Check the if the form has content. If all fields are not filled correctly, display an error and stop
     * form from sending.
    */
    function formcheck(){
        var x = document.forms["category"]["cat_name"].value;
        if (x == null || x == "") {
            alert("Category name must be filled out");
            return false;
        }
        
        var y = document.forms["category"]["cat_description"].value;
            if (y == null || y == "") {
            alert("Category description must be filled out");
            return false;
        }
    
    }
    
</script>