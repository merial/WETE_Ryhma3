<?php 
/**
* Reply to topic
* 
* This file is for creating a reply to a specific topic. The topic id is sent by the GET-method. User must be signed in 
* to reply to topics.
* 
* @author Meri Alho
* @package forum
*/

include 'header.php';

echo '<h2>Reply to topic</h2>';
if($_SESSION['signed_in'] == false){
    //the user is not signed in
    echo 'Sorry, you have to be <a href="signin.php">signed in</a> to reply to topics.';
}
else{

echo '<form method="post" action="reply.php?id='. $_GET['id'].'" name="replyform" onsubmit="return formcheck()">
    <textarea name="reply-content"></textarea><br>
    <input type="submit" value="Submit reply" />
</form>';
}
include 'footer.php';
?>

<script>
    
    /**
     * Check the if the form has content. If all fields are not filled correctly, display an error and stop
     * form from sending.
    */
    function formcheck(){
        var x = document.forms["replyform"]["reply-content"].value;
        if (x == null || x == "") {
            alert("You must write something to the reply!");
            return false;
        }

    }
    
</script>