<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="A short description." />
    <meta name="keywords" content="put, keywords, here" />
    <title>Webforum</title>
    <link rel="stylesheet" href="styles.css" type="text/css">
</head>
<body>
<h1>WETE Forum</h1>
    <div id="wrapper">
    <div id="menu">
        <a class="item" href="index.php">Home</a> 
        <a class="item" href="create_topic.php">Create a topic</a> 
        <a class="item" href="create_cat.php">Create a category</a>
         
        <div id="userbar">
        <?php
        /**
        * Header for all pages
        * 
        * This file generates the top part of the webpage. This is used in all the other files.
        * Check if the user is signed in. If signed in, display "Sign out" -option.
        * If not signed in, "create account" and "Sign in" -options are displayed. 
        * 
        * @author Meri Alho
        * @package forum
        */
        
        /** Continue signed in -session if it exists */
        session_start();
            if($_SESSION['signed_in']){
                echo 'Hello  ' . $_SESSION['user_name'] . '! | <a href="signout.php">Sign out</a>';
            }
            else{
                echo '<a href="signin.php">Sign in</a> or <a href="signup.php">create an account</a>.';
            } 
            ?>
        </div>
    </div>
        <div id="content">