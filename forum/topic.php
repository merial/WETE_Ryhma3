<?php
/**
* Display topic 
* 
* Displays the chosen topic and all the messages in it.
* 
* @author Meri Alho
* @package forum
*/

/** include MySQL-connection and header */
include 'connect.php';
include 'header.php';

/** Continue signed in -session */
session_start();


/** @var string $sql    MySQL querystring to get the topics. Select the topic based on $_GET['id']. */
$sql = "SELECT
        topic_id,
        topic_subject,
        topic_date,
        topic_cat
    FROM
        topics
    WHERE
        topics.topic_id ="  . mysql_real_escape_string($_GET['id']);

/** @var string $result     Result gotten from the MySQL query. */        
$result = mysql_query($sql);


 
if(!$result){
    echo 'The topic could not be displayed, please try again later. <br><br>' . mysql_error();
}

else{
            while($row = mysql_fetch_assoc($result)){
            echo '<h2>Topic name: ' . $row['topic_subject'] . '</h2>'; 
            $catid = $row['topic_cat'];

            }
            
            /** @var string $sql    MySQL querystring to get the chosen category name in which the topic is. */
            $sql = "SELECT
                cat_name
            FROM
                categories
            WHERE
                cat_id ="  . $catid;
        
            /** @var string $resultcat     Result gotten from the MySQL query. */
            $resultcat = mysql_query($sql);
            
            while($row = mysql_fetch_assoc($resultcat)){
                echo '<a href="category.php?id='. $catid.'">Back to '. $row['cat_name'] .'</a> | ' ;

            }
            echo '<a href="replyform.php?id='. $_GET['id'] .'">Post a reply to topic</a>';
            

        
        
        
         //prepare the table
          echo '<table border="1">
         <tr>
           <th>Info</th>
           <th>Comments</th>
         </tr>'; 
        
        /** @var string $sql    MySQL querystring to get all the topic content to display. */
        $sql = "SELECT
                    posts.post_topic,
                    posts.post_content,
                    posts.post_date,
                    posts.post_by,
                    users.user_id,
                    users.user_name
                FROM
                    posts
                LEFT JOIN
                    users
                ON
                    posts.post_by = users.user_id
                WHERE
                    posts.post_topic =' " . mysql_real_escape_string($_GET['id']) . " ' ";
        
        /** @var string $result     Result gotten from the MySQL query. */
        $result = mysql_query($sql);
        
        while($row = mysql_fetch_assoc($result)){               
            echo '<tr>';
            echo '<td class="rightpart">';
                echo 'Posted by: ' .$row['user_name']. '<br><br>';
                echo 'Posted on: '. date('d-m-Y', strtotime($row['post_date'])).'<br>';
                echo 'Posted at: '. date('H:i:s', strtotime($row['post_date']));
            echo '</td>';

            echo '<td class="leftpart">';
                echo $row['post_content'];
            echo '</td>';
            echo '</tr>';
        }
        

    
    
    }
        
        

include 'footer.php';
?>