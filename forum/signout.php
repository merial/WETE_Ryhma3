<?php
/**
* User sign out
* 
* Signs the user out (destroys the session) and removes all session variables from signed-in session. 
* 
* @author Meri Alho
* @package forum
*/

include 'connect.php';
include 'header.php';
 
// remove all session variables
session_unset(); 

// destroy the session 
session_destroy(); 

echo "You have signed out. <br><br>";
echo '<a href="signin.php">Sign in</a> or <a href="signup.php">create an account</a>.';

include 'footer.php';
?>