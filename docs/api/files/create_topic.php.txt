<?php
/**
* @author Meri Alho
* 
* This file is for creating a new topic into a category. Category to create the topic in must be chosen from the dropdown menu.
* 
* 
*/

include 'connect.php';
include 'header.php';

/** Continue signed in -session */
session_start();

echo '<h2>Create a topic</h2>';
if($_SESSION['signed_in'] == false){
    //the user is not signed in
    echo 'Sorry, you have to be <a href="signin.php">signed in</a> to create a topic.';
}
else{
    //the user is signed in
    if($_SERVER['REQUEST_METHOD'] != 'POST'){   
        //the form hasn't been posted yet, display it
        //retrieve the categories from the database for use in the dropdown
        
        /** @var string $sql    MySQL query string to be excecuted. Retrieves category information */
        $sql = "SELECT
                    cat_id,
                    cat_name,
                    cat_description
                FROM
                    categories";
         
        /**  @var string $result    Result of the previous MySQL query */
        $result = mysql_query($sql);
         
        if(!$result){
            //the query failed
            echo 'Error while selecting from database. Please try again later.';
        }
        else{
            if(mysql_num_rows($result) == 0){
                //there are no categories, so a topic can't be posted
                if($_SESSION['user_level'] == 1){
                    echo 'You have not created categories yet.';
                }
                else{
                    echo 'Before you can post a topic, you must wait for an admin to create some categories.';
                }
            }
            else{
         
                echo '<form method="post" action="" name="topic" onsubmit="return formcheck()">
                    Subject: <input type="text" name="topic_subject" /><br>
                    Category: '; 
                 
                 //make a dropdown menu for the categories
                echo '<select name="topic_cat">';
                    while($row = mysql_fetch_assoc($result)){
                        echo '<option value="' . $row['cat_id'] . '">' . $row['cat_name'] . '</option>';
                }
                echo '</select><br>'; 
                     
                echo 'Message: <br><textarea name="post_content" /></textarea><br>
                    <input type="submit" value="Create topic" />
                 </form>';
            }
        }
    }
    else{
        //start the transaction
        /** @var string $query MySQL query to be excecuted */ 
        $query  = "BEGIN WORK;";
        
        /**  @var string $result    MySQL query result */
        $result = mysql_query($query);
         
        if(!$result){
            //Tthe query failed, quit
            echo 'An error occured while creating your topic. Please try again later.';
        }
        else{
     
            //the form has been posted, so save it
            //insert the topic into the topics table first, then we'll save the post into the posts table
            
            //first remove html tags
            /** @var string $posttopicsubject       Get topic subject from form to a variable with POST-method and remove all html tags from it. */
            $posttopicsubject = strip_tags($_POST['topic_subject']);
            
            /** @var string $topiccat       Get topic category from form to a variable with POST-method and remove all html tags from it. */
            $topiccat = strip_tags($_POST['topic_cat']);
            
            /** @var string $sql    MySQL query string to be excecuted. mysql_real_escape_string() used for variables got from the form to avoid database injection. */
            $sql = "INSERT INTO 
                        topics(topic_subject,
                               topic_date,
                               topic_cat,
                               topic_by)
                   VALUES('" . mysql_real_escape_string($posttopicsubject) . "',
                               NOW(),
                               " . mysql_real_escape_string($topiccat) . ",
                               " . $_SESSION['user_id'] . "
                               )";
            /** @var string $result    Result of the MySQL query */          
            $result = mysql_query($sql);
            
            if(!$result){
                //something went wrong, display the error
                echo 'An error occured while inserting your data. Please try again later.' . mysql_error();
                $sql = "ROLLBACK;";
                $result = mysql_query($sql);
            }
            else{
                //the first query worked, now start the second, posts query
                //retrieve the id of the freshly created topic for usage in the posts query
                /**  @var integer $topicid      Retrieve the latest created topic id for usage in the posts query. */
                $topicid = mysql_insert_id();
                
                //sanitize post content for html tags
                /** @var string $postcontent      Get topic content from form to a variable with POST-method and remove all html tags from it. */
                $postcontent = strip_tags($_POST['post_content']);
                
                /** @var string $sql    MySQL query string. Insert the new topic to the database. */ 
                $sql = "INSERT INTO
                            posts(post_content,
                                  post_date,
                                  post_topic,
                                  post_by)
                        VALUES
                            ('" . mysql_real_escape_string($postcontent) . "',
                                  NOW(),
                                  " . $topicid . ",
                                  " . $_SESSION['user_id'] . "
                            )";
                /**  @var $result       Result for the MySQL query. */
                $result = mysql_query($sql);
                 
                if(!$result){
                    //something went wrong, display the error
                    echo 'An error occured while inserting your post. Please try again later.' . mysql_error();
                    
                    /** @var string $sql    MySQL query to be made. Reverse the latest query if it did not succeed. */ 
                    $sql = "ROLLBACK;";
                    
                    /**  @var $result       Result for the MySQL query. */
                    $result = mysql_query($sql);
                }
                else{
                    /**  @var $sql       MySQL query to be made. Commit changes to the database now that the last query succeeded. */
                    $sql = "COMMIT;";
                    
                    /**  @var $result       Result for the MySQL query. */
                    $result = mysql_query($sql);
                     
                    //the query succeeded
                    echo 'You have successfully created <a href="topic.php?id='. $topicid . '">your new topic</a>.';
                }
            }
        }
    }
}
 
include 'footer.php';
?>

<script>
    
    /**
     * Check the if the form has content. If all fields are not filled correctly, display an error and stop
     * form from sending.
    */
    function formcheck(){
        var x = document.forms["topic"]["topic_subject"].value;
        if (x == null || x == "") {
            alert("Topic subject must be filled out");
            return false;
        }

    }
    
</script>
